import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

'Select Hospital'
switch (v_Facility) {
    case 'Tokyo':
        WebUI.selectOptionByIndex(findTestObject('Page-Make Appointment/slc_Facility'), 0)

        break
    case 'Hongkong':
        WebUI.selectOptionByIndex(findTestObject('Page-Make Appointment/slc_Facility'), 1)

        break
    case 'Seoul':
        WebUI.selectOptionByIndex(findTestObject('Page-Make Appointment/slc_Facility'), 2)

        break
}

if (v_Apply_Hospital_Readmission.equals('1')) {
    WebUI.check(findTestObject('Object Repository/Page-Make Appointment/chk_Hospital readmission'))
}

'Select Healthcare Program'
switch (v_Facility) {
    case 'Medicare':
        WebUI.selectOptionByIndex(findTestObject('Page-Make Appointment/slc_Facility'), 0)

        break
    case 'Medicaid':
        WebUI.selectOptionByIndex(findTestObject('Page-Make Appointment/slc_Facility'), 1)

        break
    case 'None':
        WebUI.selectOptionByIndex(findTestObject('Page-Make Appointment/slc_Facility'), 2)

        break
}

WebUI.setText(findTestObject('Object Repository/Page-Make Appointment/txt_Visit Date'), v_Visit_Date)

WebUI.setText(findTestObject('Page-Make Appointment/txt_Comment'), v_Comment)

WebUI.click(findTestObject('Page-Make Appointment/btn_Book Appointment'))

