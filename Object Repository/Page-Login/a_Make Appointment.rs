<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Make Appointment</name>
   <tag></tag>
   <elementGuidId>1842bc73-5799-4db2-8cc2-c5df66738e12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#btn-make-appointment</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='btn-make-appointment']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>86fa4103-3007-4c10-84f1-7bc239e1c09a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btn-make-appointment</value>
      <webElementGuid>466c72e4-4e8e-4376-b2f5-168ec179f85a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>./profile.php#login</value>
      <webElementGuid>5ffd7090-34f5-4f91-a17e-1fd48e010c99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-dark btn-lg</value>
      <webElementGuid>a0491480-dad7-44a0-8b6f-f00976975d2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Make Appointment</value>
      <webElementGuid>baacac96-bc11-4974-b230-98b698362f96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btn-make-appointment&quot;)</value>
      <webElementGuid>30fe9400-2c42-43bd-97d1-a60a120dea84</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='btn-make-appointment']</value>
      <webElementGuid>d533a20f-02ee-4efb-a775-0ae51abd325d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//header[@id='top']/div/a</value>
      <webElementGuid>3711749e-692a-49c2-ae0d-19ca60ccfc08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Make Appointment')]</value>
      <webElementGuid>4a2acfa8-1993-462f-9a24-5e79965753af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::a[1]</value>
      <webElementGuid>caa4da28-8ff8-4d78-9579-291fd2032f97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CURA Healthcare Service'])[2]/following::a[1]</value>
      <webElementGuid>288deb80-6f77-4d4a-a681-ef50f6e85efc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CURA Healthcare Service'])[3]/preceding::a[1]</value>
      <webElementGuid>d2aa92cf-e030-4a97-97d6-eb2d58704adc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(678) 813-1KMS'])[1]/preceding::a[1]</value>
      <webElementGuid>a03050fd-7c38-42fe-9fd8-2492f74bf1ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Make Appointment']/parent::*</value>
      <webElementGuid>249d953f-f85a-4408-bc1d-d59ca7194c63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, './profile.php#login')]</value>
      <webElementGuid>e920d271-a9e3-46c0-a45a-d1ea8bd63dd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/a</value>
      <webElementGuid>c88aeffc-34cc-4d4c-b0e4-06fa70b8c203</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'btn-make-appointment' and @href = './profile.php#login' and (text() = 'Make Appointment' or . = 'Make Appointment')]</value>
      <webElementGuid>5f771a4e-4634-4096-bf98-a23555a811fd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
