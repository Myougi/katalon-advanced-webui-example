<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Appointment Confirmation</name>
   <tag></tag>
   <elementGuidId>b8bac761-e284-48e3-a2fb-12023f7d2c99</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='summary']/div/div/div/h2</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>365ce2d5-9440-4de1-a7ea-e89cd136d76e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Appointment Confirmation</value>
      <webElementGuid>26279e0e-f296-47f1-aa8e-bd7c5ff48052</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;summary&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 text-center&quot;]/h2[1]</value>
      <webElementGuid>18dca013-deef-46c0-a2e2-82b47eea269b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='summary']/div/div/div/h2</value>
      <webElementGuid>b74f9684-7902-454a-a809-4d5371cb0945</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::h2[1]</value>
      <webElementGuid>bb8efa85-6a81-4c40-958c-61b2343c951b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::h2[1]</value>
      <webElementGuid>17235f1d-9507-4d94-a267-13171ea85aae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Facility'])[1]/preceding::h2[1]</value>
      <webElementGuid>22d50a7d-d890-42ce-874d-2f3771a38b25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply for hospital readmission'])[1]/preceding::h2[1]</value>
      <webElementGuid>a0e8be29-28c7-4868-99fa-023f08ffd5bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Appointment Confirmation']/parent::*</value>
      <webElementGuid>e712158f-ff75-43d4-94c0-a71e33ecf215</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
      <webElementGuid>1b78c4b9-7411-48e0-8f3c-2a7a458920f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Appointment Confirmation' or . = 'Appointment Confirmation')]</value>
      <webElementGuid>03b1d1e4-0e41-446f-9056-ab019b4510bb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
